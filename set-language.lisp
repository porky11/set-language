(defpackage #:set-language
  (:use #:cl #:alexandria #:split-sequence))

(in-package #:set-language)

(defpackage #:set-language-primitives
  (:nicknames #:slp)
  (:export #:unite
           #:imply
           #:intersect
           #:differ
           #:equal
           #:relate))

(defun split-string (string split)
  (dotimes (i (length string))
    (when (char= (char string i) split)
      (return-from split-string
        (values (subseq string 0 i) (subseq string (1+ i))))))
  string)

(defparameter *binary* (plist-hash-table '("is" slp:imply
                                           "or" slp:differ
                                           "and" slp:unite
                                           "be" slp:equal
                                           "of" slp:relate)
                                         :test #'equal))

(defparameter *implicit*
  (list "a" "one" "the"
        "to" "from" "with" "at" "on" "in" "after" "before"
        "who" "if"))

(defparameter *break* (plist-hash-table '("so" slp::relate
                                          "e" slp::relate
                                          "te" slp:intersect)
                                        :test #'equal))

(defparameter *reassociate* (plist-hash-table '("then" "if")
                                              :test #'equal))



(defun parse-sentence (tokens &optional (kind t) &aux dep)
  (do ((token (car tokens) (car rest))
       (rest (cdr tokens) (cdr rest)))
      ((not token) dep)
    (when-let ((reassociate (gethash token *break*)))
      (unless (eq kind reassociate)
        (push token rest))
      (return-from parse-sentence (values dep rest)))
    (if-let ((reassociate (gethash token *reassociate*)))
      (unless (and (listp dep) (equal (cadr dep) reassociate))
        (return-from parse-sentence (values dep (cons token rest))))
      (multiple-value-setq (dep rest)
        (if-let ((op (gethash token *binary*)))
          (multiple-value-bind (next rest) (parse-sentence rest op)
            (values (list op dep next) rest))
          (if dep
              (multiple-value-bind (next rest) (parse-sentence (cons token rest) 'slp:intersect)
                (values (list 'slp:intersect dep next) rest))
              (if (member token *implicit* :test #'string=)
                  (multiple-value-bind (next rest) (parse-sentence rest 'slp:relate)
                    (values (list 'slp:relate token next) rest))
                  (values token rest))))))))

(defun parse-text (string)
  (multiple-value-bind (first rest) (split-string string #\.)
    (let ((sentence (parse-sentence (split-sequence #\Space first))))
      (if (and rest (string\= rest ""))
          `(slp:intersect sentence ,(parse-text rest))
          sentence))))





